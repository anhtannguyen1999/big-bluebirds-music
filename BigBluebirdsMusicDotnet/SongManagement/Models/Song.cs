using System;

namespace SongManagement
{
    public class Song
    {
        public int ID { get; set; }

        public String Name { get; set; }

        //public int TypeID { get; set; }

        public DateTime DateCreate { get; set; } 

        public int TottalListen { get; set; } 

        public int TotalLike { get; set; } 

        public int TotalCmt { get; set; } 

        public String Lyric { get; set; }

        public int Duration { get; set; } 
    }
}
