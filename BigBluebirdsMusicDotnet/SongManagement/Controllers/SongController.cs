﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace SongManagement.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SongController : ControllerBase
    {
        
        private readonly ILogger<SongController> _logger;

        public SongController(ILogger<SongController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<Song> Get()
        {
            return Enumerable.Range(1, 5).Select(index => new Song
            {
                ID = 1,
                Name = "Hoa Hai Duong",
                DateCreate = DateTime.Now
            })
            .ToArray();
        }
    }
}
